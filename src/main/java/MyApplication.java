import jakarta.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/api/service")
public class MyApplication extends ResourceConfig {
    public MyApplication() {
        packages("controllers");
        packages("filters");
    }
}
