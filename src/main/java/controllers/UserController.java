package controllers;

import domain.models.User;
/*
import filters.customAnnotations.JWTTokenNeeded;
import filters.customAnnotations.OnlyAdmin;
 */

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import services.UserService;
import services.interfaces.IUserService;

import java.awt.*;
import java.util.List;

@Path("/user")
public class UserController {
    private IUserService userservice;
    private String sqlstmt = "SELECT * FROM users";

    public UserController() {
        userservice = new UserService();
    }


    @GET
    @Path("/getall")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        List<User> users;
        try {
            users = userservice.getAll();
        }
        catch (ServerErrorException ex) {
            return Response
                    .serverError()
                    .entity(ex.getMessage())
                    .build();
        }
        catch (BadRequestException ex) {
            return Response
                    .serverError()
                    .entity(ex.getMessage())
                    .build();
        }

        if(users.size() != 0) {
            return Response
                    .ok(users)
                    .build();
        } else {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .build();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("id") int id) {
        User user;
        try {
            user = userservice.getUserByID(id);
        }
        catch (ServerErrorException ex) {
            return Response
                    .serverError()
                    .entity(ex.getMessage())
                    .build();
        }
        catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage())
                    .build();
        }

        if(user != null) {
            return Response
                    .ok(user)
                    .build();
        } else {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .build();
        }
    }

    @GET
    @Path("/number")
    public Response getNumber() {
        int number = 0;
        try {
            number = userservice.getNumber();
        }
        catch (ServerErrorException ex) {
            return Response
                    .serverError()
                    .entity(ex.getMessage())
                    .build();
        }
        catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage())
                    .build();
        }
        return Response
                .ok(number)
                .entity("Number of Users : " + number)
                .build();
    }

    @POST
    @Path("/adduser")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addUser(User user) {
        try {
            userservice.addUser(user);
        }
        catch (ServerErrorException ex) {
            return Response
                    .serverError()
                    .entity(ex.getMessage())
                    .build();
        }
        catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage())
                    .build();
        }

        return Response
                .status(Response.Status.CREATED)
                .entity("User has been created")
                .build();
    }

    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUser(User user) {
        try {
            userservice.updateUser(user);
        }
        catch (ServerErrorException ex) {
            return Response
                    .serverError()
                    .entity(ex.getMessage())
                    .build();
        }
        catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage())
                    .build();
        }

        return Response
                .ok("User has been updated successfully!")
                .build();
    }

    @POST
    @Path("/remove")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response removeUser(User user) {
        try {
            userservice.remove(user);
        }
        catch (ServerErrorException ex) {
            return Response
                    .serverError()
                    .entity(ex.getMessage())
                    .build();
        }
        catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage())
                    .build();
        }

        return Response
                .ok("User has been deleted successfully")
                .build();
    }

}
