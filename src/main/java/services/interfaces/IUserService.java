package services.interfaces;

import domain.models.User;

import java.util.List;

public interface IUserService {
    User getUserByID(long id);

    User getUserByUsername(String username);

    void addUser(User user);

    void updateUser(User user);

    List<User> getAll();

    int getNumber();

    void remove(User user);
}
