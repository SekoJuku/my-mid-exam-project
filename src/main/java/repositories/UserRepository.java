package repositories;

import domain.models.User;
import domain.UserLoginData;
import jakarta.ws.rs.BadRequestException;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IEntityRepository;
import repositories.interfaces.IUserRepository;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class UserRepository implements IUserRepository {
    private Connection dbrepo;
    private String sqlstmt = "SELECT * FROM users";

    public UserRepository() { dbrepo = PostgressRepository.getConnection(); }

    @Override
    public void add(User entity) {
        try {
            String sql = "INSERT INTO users(name, surname, username, password, birthday) " +
                    "VALUES(?, ?, ?, ?, ?)";
            PreparedStatement stmt = dbrepo.prepareStatement(sql);
            stmt.setString(1, entity.getName());
            stmt.setString(2, entity.getSurname());
            stmt.setString(3, entity.getUsername());
            stmt.setString(4, entity.getPassword());
            stmt.setDate(5, entity.getBirthday());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void update(User entity) {
        String sql = "UPDATE users " +
                "SET ";
        int c = 0;
        if (entity.getName() != null) {
            sql += "name=?, "; c++;
        }
        if (entity.getSurname() != null) {
            sql += "surname=?, "; c++;
        }
        if (entity.getPassword() != null) {
            sql += "password=?, "; c++;
        }
        if (entity.getBirthday() != null) {
            sql += "birthday=?, "; c++;
        }

        sql = sql.substring(0, sql.length() - 2);

        sql += " WHERE username = ?";

        try {
            int i = 1;
            PreparedStatement stmt = dbrepo.prepareStatement(sql);
            if (entity.getName() != null) {
                stmt.setString(i++, entity.getName());
            }
            if (entity.getSurname() != null) {
                stmt.setString(i++, entity.getSurname());
            }
            if (entity.getPassword() != null) {
                stmt.setString(i++, entity.getPassword());
            }
            if (entity.getBirthday() != null) {
                stmt.setDate(i++, entity.getBirthday());
            }
            stmt.setString(i++, entity.getUsername());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void remove(User entity) {
        try {
            String sql = "DELETE * FROM users WHERE id = ?";
            PreparedStatement stmt = dbrepo.prepareStatement(sql);
            stmt.setInt(1,entity.getId());
            ResultSet rs = stmt.executeQuery();


        } catch (SQLException ex) {
            throw new BadRequestException("User has not been removed successfully!");
        }
    }

    @Override
    public List<User> query(String sql) {
        try {
            Statement stmt = dbrepo.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<User> users = new LinkedList<>();
            while (rs.next()) {
                User user = new User(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        //rs.getString("password"),
                        rs.getDate("birthday")
                );
                users.add(user);
            }
            return users;
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getSQLState());
        }
    }

    @Override
    public User queryOne(String sql) { // ОРМ ????
        try {
            Statement stmt = dbrepo.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return new User(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getDate("birthday")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    public User getUserByID(long id) {
        String sql = "SELECT * FROM users WHERE id = " + id + " LIMIT 1";
        return queryOne(sql);
    }

    public User findUserByLogin(UserLoginData data) {
        try {
            String sql = "SELECT * FROM users WHERE username = ? AND password = ?";
            PreparedStatement stmt = dbrepo.prepareStatement(sql);
            stmt.setString(1, data.getUsername());
            stmt.setString(2, data.getPassword());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new User(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getDate("birthday")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    public User getUserByUsername(String username) {
        try {
            String sql = "SELECT * FROM users WHERE username = ?";
            PreparedStatement stmt = dbrepo.prepareStatement(sql);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new User(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getDate("birthday")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    @Override
    public List<User> getAll() {
        return query(sqlstmt);
    }

    @Override
    public int getNumber() {
        try {
            String sql = "SELECT COUNT(DISTINCT id) as result FROM users";
            PreparedStatement stmt = dbrepo.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs.getInt("result");
            }
        }
        catch (SQLException ex) {
            throw new BadRequestException();
        }
        return 0;
    }
}
