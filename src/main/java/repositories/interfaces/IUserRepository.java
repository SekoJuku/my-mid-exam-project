package repositories.interfaces;

import domain.UserLoginData;
import domain.models.User;

import java.util.List;

public interface IUserRepository extends IEntityRepository<User> {
    User getUserByID(long id);

    User findUserByLogin(UserLoginData data);

    User getUserByUsername(String username);

    List<User> getAll();

    int getNumber();
}
