package repositories;

import jakarta.ws.rs.InternalServerErrorException;
import repositories.interfaces.IDBRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgressRepository implements IDBRepository {
    private PostgressRepository() { }

    public static Connection getConnection() {
        try {
            return DriverManager.getConnection("jdbc:postgresql://localhost:5432/final","postgres","123");
        }
        catch (SQLException error) {
            throw new InternalServerErrorException();
        }
    }
}
